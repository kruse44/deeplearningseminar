## DeepLearningSeminar
Dieses Repo dient als Sammelbecken des im Lauf der Vorbereitung angefallenen Codes. Der entsprechende Latex-Code (ohne Grafiken allerdings mit verweis auf die zu Grunde liegende Ordnerstruktur), sowie der genutze Python code zur Visualisierung des Stochastic Gradient Descents sind hier zu finden.

