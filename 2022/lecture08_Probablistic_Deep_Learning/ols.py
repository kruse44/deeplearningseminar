# Libraries:
import random
import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pylab as plt


# Set seed:
random.seed(42)

# Daten erstellen:
X = np.random.normal(loc=1.0, scale=1.0, size = 250)
X = X.reshape(250, 1)
# beta_0
koef = np.array((3))
# Residuen
resid = np.random.normal(loc=0.0, scale=1.0, size=250)
resid = resid.reshape(250, 1)
# y berechnen mit intercept 1.5
y = 1 + X * koef + resid

# Modelle ersetllen:

# ML
model_ml = tf.keras.models.Sequential([
    tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)),
])

def f(y, mu, sigma=1):
  return (1/(sigma * tf.math.sqrt(2 * np.pi)) * tf.math.exp( - (y - mu)**2 / (2 * sigma**2)))

def my_loss(y_true,y_pred):
    loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred)))
    return loss

model_ml.compile(
    optimizer='sgd',
    loss = my_loss,
)

model_ml.fit(X, y, batch_size=1, epochs=50, verbose=2)
model_ml.summary()
model_ml.get_weights()
