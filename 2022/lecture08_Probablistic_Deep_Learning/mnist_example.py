# Imports:
from sklearn import metrics
import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import time 
from sklearn.metrics import confusion_matrix

# Laden der Daten:
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
# separate x_train in X_train and X_val, same for y_train
X_train=x_train[0:50000] / 255 #divide by 255 so that they are in range 0 to 1
Y_train=tf.keras.utils.to_categorical(y_train[0:50000],10) # one-hot encoding

X_val=x_train[50000:60000] / 255
Y_val=tf.keras.utils.to_categorical(y_train[50000:60000],10)

X_test=x_test / 255
Y_test=tf.keras.utils.to_categorical(y_test,10)

del x_train, y_train, x_test, y_test

X_train=np.reshape(X_train, (X_train.shape[0],28,28,1))
X_val=np.reshape(X_val, (X_val.shape[0],28,28,1))
X_test=np.reshape(X_test, (X_test.shape[0],28,28,1))

# prepare data for fcNN - we need a vector as input

X_train_flat = X_train.reshape([X_train.shape[0], 784])
X_val_flat = X_val.reshape([X_val.shape[0], 784])
X_test_flat = X_test.reshape([X_test.shape[0], 784])

# Model erstellen:
model = tf.keras.models.Sequential([
    tf.keras.layers.Dense(100, batch_input_shape=(None, 784)),
    tf.keras.layers.Activation('relu'),
    tf.keras.layers.Dense(50),
    tf.keras.layers.Activation('relu'),
    tf.keras.layers.Dense(10),
    tf.keras.layers.Activation('softmax')
])

# Model compile:
model.compile(
    optimizer = tf.keras.optimizers.Adam(0.001),
    loss = 'categorical_crossentropy',
    metrics = ['accuracy'],
)

# Model anschauen:
model.summary()

# Model fitten:
model.fit(X_train_flat, Y_train,
          batch_size=128,
          epochs=10, 
          verbose=2,
          validation_data=(X_val_flat, Y_val)
          )

# Anschauen der Ergebnisse:
pred = model.predict(X_test_flat)
print(confusion_matrix(np.argmax(Y_test, axis=1), np.argmax(pred, axis=1)))
np.sum(np.argmax(Y_test, axis=1) == np.argmax(pred, axis=1))/len(pred)

Y_test[0]
pred[0]