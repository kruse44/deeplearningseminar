# Simple example: OLS in Tensorflow:

import pandas as pd
import os as os
os.add_dll_directory("C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v11.2/bin")
import tensorflow as tf

# Set number of observations and samples
N = 1000

# Simulate the data:
# True values :
beta0 = tf.constant([1.0])
beta1 = tf.constant([3.0])

# Draw independent variable and error.
X = tf.random.normal([N, 1])
residuals = tf.random.normal([N, 1], stddev=1.)

# Compute dependent variable.
Y = beta0 + beta1 * X + residuals

# Create Target variables:
beta0Hat = tf.Variable(0., name='intercept')
beta1Hat = tf.Variable(0., name='beta1')

# Define the loss function
def MSE(beta0Hat, beta1Hat, x_Sample, y_Sample):
    y_hat = beta0Hat + beta1Hat * x_Sample
    mse = tf.reduce_mean(tf.square(y_Sample - y_hat))
    return mse

# Define optimizer.
opt = tf.optimizers.SGD()

# Optimizing:
for j in range(1000):
    # Perform minimization step.
    opt.minimize(
        lambda: MSE(beta0Hat=beta0Hat, beta1Hat=beta1Hat, x_Sample = X[:, 0], y_Sample = Y[:, 0]),
        var_list=[beta0Hat, beta1Hat],
    )