import numpy as np
import pandas as pd

# Gewichte
w_ih = pd.DataFrame(np.array([[0.1, 0.1, 0.1], [0.2, 0.2, 0.2], [0.3, 0.3, 0.3]]))
w_ho = pd.DataFrame(np.array([[0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9]]))

# Bias
bih = 0.1
bho = 0.1

# Activierungsfunktionen
def sigma_ih(x):
    temp = max(0, x)
    return temp 

def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)
    
# Input
i = pd.DataFrame(np.array([[0.1], [0.3], [0.7]]))

# True values
y = np.array([0.0, 0.0, 1.0])

# Feedforward

## I to H
h = np.matmul(w_ih, i) + bih

## H to o
temp_o = np.matmul(w_ho, h) + bho

softmax(np.matmul(w_ho, h) + bho)

# Loss:
round(- 1/3 * ((0 * np.log(0.243) + (1- 0) * np.log(1 - 0.243)) + (0 * np.log(0.324) + (1- 0) * np.log(1 - 0.324)) + (1 * np.log(0.433) + (1- 1) * np.log(1 - 0.433))), 3 )


def difloss(y, yhat):
    temp = -1 * (y * (1 / yhat) + (1 - y) * ( 1 / (1 - yhat)))
    out = round(temp, 3)
    return out

difloss(y = 0, yhat = 0.243)
difloss(y = 0, yhat = 0.324)
difloss(y = 0, yhat = 0.433)